**[0.7.0] 2019-01-03**
- **Huge** improvement in resource utilization thanks to the switch from CLI tools to libs to read the data 
- Historical data dialog! (Requires GNOME 3.30+. New dependency necessary, check the Distribution dependencies section of the readme)
- Highlighting currently applied profile

**[0.5.1] 2019-01-03**
 - Min duty changed from 25 to 0
 - Slightly bigger app indicator icon

**[0.5.0] 2019-01-03**
 - Implemented Fan curve profile! 
 - Added GPU temperature to app indicator
 - Changed app indicator icon (again)
 - Other minor UI improvements

**[0.3.2] 2019-01-01**
 - Fixed overclock slider value not correctly restored on startup

**[0.3.1] 2019-01-01**
 - Removed unnecessary dependency from py3nvml
 
**[0.3.0] 2019-01-01**
 - Enabled version check on startup
 - Added ability to overclock GPU and Memory (requires coolbits to be set!)
 - Added ability to change Power Limit
 - Changed app indicator icon
 - Other minor UI improvements

**[0.1.0] 2018-12-31**
 - Initial release